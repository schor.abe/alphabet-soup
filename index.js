const http = require('http');
const fs = require('fs');
const express = require("express");
const hostname = '127.0.0.1';
const port = 3000;
const app = express();
const bodyParser = require('body-parser');
const path = require("path");
const fileupload = require("express-fileupload");

app.use(fileupload());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.post('/', (req, res) => {
	const buf = Buffer.from(req.files.file.data, 'utf8');
	const [puzzle, words, width, height] = parse(buf.toString());
	const results = search(puzzle, words);

  res.send(results.join("</p><p>"));
});

app.listen(port);

const directions = [
	(x, y) => [x, y + 1],
	(x, y) => [x, y - 1],
	
	(x, y) => [x - 1, y],
	(x, y) => [x + 1, y],
	
	(x, y) => [x + 1, y + 1],
	(x, y) => [x - 1, y - 1],

	(x, y) => [x - 1, y + 1],
	(x, y) => [x + 1, y - 1],
];

/**
 * Parses the puzzle file.
 * @param  String data Raw file data.
 * @return Array  The file data parsed.
 */
function parse(data) {
	const parts = data.split(/\r\n|\n\r|\n|\r/);
	const [dimentions, ...otherData] = parts;
	const [width, height] = dimentions.split('x');

	const puzzle = otherData.slice(0, height).map((line) => line.split(' '));
	const words = otherData.slice(height).filter(n => n);
	
	return [puzzle, words, width, height];
}

/**
 * Searches the puzzle for each word.
 * @param  Array  puzzle Puzzle data.
 * @param  String word   Word we are searching for.
 * @return Array  The cords for each word.
 */
function search(puzzle, words) {
	const output = [];
	words.forEach(word => {
		const chars = word.split("");
		output.push(findWord(puzzle, chars));
	});

	return output;
}

/**
 * Finds one word in the puzzle
 * @param  Array puzzle Puzzle data.
 * @param  Array word   The word split in an array of charecters.
 * @return String The cords results.
 */
function findWord(puzzle, word) {
	for (let y = 0; y < puzzle.length; y++) {
		for (let x = 0; x < puzzle[0].length; x++) {
			if (puzzle[x][y] == word[0]) {
				const match = checkDirections(puzzle, word, x, y);
				if (match !== false) {
					return match;
				}
			}
		}
	}
}


/**
 * Checks all possible direction functions to see if we can find the word, given a starting point.
 * @param  Array  puzzle Puzzle data.
 * @param  Array  word   The word split in an array of charecters.
 * @param  Number startX The x cord were we have the first charecter matching the word we are looking for.
 * @param  Number startY The y cord were we have the first charecter matching the word we are looking for.
 * @return False|String
 */
function checkDirections(puzzle, word, startX, startY) {
	for (let i = 0; i < directions.length; i ++) {
		const match = checkDirection(puzzle, word, startX, startY, 1, directions[i]);
		if (match != false) {
			return `${word.join("")} ${startX}:${startY} ${match[0]}:${match[1]}`;
		}
	}

	return false;
}

/**
 * Checks for a match recursivly, given starting cords and a direction function.
 * @param  Array puzzle  Puzzle data.
 * @param  Array  word   The word split in an array of charecters.
 * @param  Number x      The x cord we are at in the puzzle.
 * @param  Number y      The y cord we are at in the puzzle.
 * @param  Number index  The index we are currently up to in the word we are checking for.
 * @param  Function dir  The direction function to use.
 * @return False|Array   returns the match end cords or false if no match was
 * found using this direction function.
 */
function checkDirection(puzzle, word, x, y, index, dir) {
	const charecter = word[index];
	const [newX, newY] = dir(x, y);

	// off the board
	if (newX < 0 || newY < 0 || newX >= puzzle[0].length || newY >= puzzle.length) {
		return false;
	}

	// not the solution
	if (charecter != puzzle[newX][newY]) return false;


	if (index >= word.length - 1) {
		// its a match!
		return [newX, newY];
	} else {
		// keep exploring this possible solution
		return checkDirection(puzzle, word, newX, newY, index + 1, dir);
	}
}